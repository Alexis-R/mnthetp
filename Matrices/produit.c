#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "produit.h"

#include "MATRICEtype.h"

//Produit d'une matrice par un float//

void prod_MP(matricePF m, float a,matricePF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m[i][j] * a;
		}
	}	
}

void prod_MPD(matricePD m, float a,matricePD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m[i][j] * a;
		}
	}
}

void prod_MG(matriceGF m, float a,matriceGF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m[i][j] * a;
		}
	}
}

void prod_MGD(matriceGD m, float a,matriceGD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m[i][j] * a;
		}
	}
}

/////////////////////////////////////////////////////////////////
