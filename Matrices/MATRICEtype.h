/*
26/05/2015
Alexis RAGUENES
Jordan ELLAPIN

MODEV =  -DPETITS=150000  -DGRANDS=500000
RANGP ->  PETITS
et
RANGG - >GRANDS


*/
#ifndef MATRICEtype

#define MATRICEtype


typedef float matricePF[PETITS][PETITS];
typedef double matricePD[PETITS][PETITS];
typedef float matriceGF[GRANDS][GRANDS];
typedef double matriceGD[GRANDS][GRANDS];



#endif


