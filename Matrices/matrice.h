


void gaxpyP(matricePF m, vecteurPF v, vecteurPF v3);
void gaxpyPD(matricePD m, vecteurPD v, vecteurPD v3);
void gaxpyG(matriceGF m, vecteurGF v, vecteurGF v3);
void gaxpyGD(matriceGD m, vecteurGD v, vecteurGD v3);

//void lu_dsmat(t_dsmat a, t_dsmat l, t_dsmat u);

void matprod_MP(matricePF m1, matricePF m2,matricePF res);
void matprod_MPD(matricePD m1, matricePD m2,matricePD res);
void matprod_MG(matriceGF m1, matriceGF m2,matriceGF res);
void matprod_MGD(matriceGD m1, matriceGD m2,matriceGD res);

void prod_MP(matricePF m, float a,matricePF res);
void prod_MPD(matricePD m, float a,matricePD res);
void prod_MG(matriceGF m, float a,matriceGF res);
void prod_MGD(matriceGD m, float a,matriceGD res);



void somme_MP(matricePF m1, matricePF m2,matricePF res);
void somme_MPD(matricePD m1, matricePD m2,matricePD res);
void somme_MG(matriceGF m1, matriceGF m2,matriceGF res);
void somme_MGD(matriceGD m1, matriceGD m2,matriceGD res);


void matrans_MP(matricePF m, matricePF mt);
void matrans_MPD(matricePD m, matricePD mt);
void matrans_MG(matriceGF m, matriceGF mt);
void matrans_MGD(matriceGD m, matriceGD mt);

