#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "mat_produit.h"
#include "MATRICEtype.h"

//Produit de 2 matrices de meme nature//

void matprod_MP(matricePF m1, matricePF m2,matricePF res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<PETITS;k++){
				tmp = tmp + (m1[j][k] * m2[k][j]);
				printf("tmp = %d\n", tmp);
			}
			res[i][j] = tmp;
			tmp = 0;
		}
	}
}

void matprod_MPD(matricePD m1, matricePD m2,matricePD res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<PETITS;k++){
				tmp = tmp + m1[j][k] * m2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

void matprod_MG(matriceGF m1, matriceGF m2,matriceGF res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<GRANDS;k++){
				tmp = tmp + m1[j][k] * m2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

void matprod_MGD(matriceGD m1, matriceGD m2,matriceGD res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<GRANDS;k++){
				tmp = tmp + m1[j][k] * m2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

/////////////////////////////////////////////////////////////////
