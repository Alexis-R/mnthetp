#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "somme.h"
#include "MATRICEtype.h"

//Somme de 2 matrices//

void somme_MP(matricePF m1, matricePF m2,matricePF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}	
}

void somme_MPD(matricePD m1, matricePD m2,matricePD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}		
}

void somme_MG(matriceGF m1, matriceGF m2,matriceGF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}		
}

void somme_MGD(matriceGD m1, matriceGD m2,matriceGD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}		
}

/////////////////////////////////////////////////////////////////
