#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "mat_produit.h"

//Test de la fonction produit matrice entre 2 matrices de même nature//

int main(){

	matriceP m1;
	m1[0][0] = 0;
	m1[0][1] = 1;
	m1[1][0] = 1;
	m1[1][1] = 5;
	matriceP m2;
	m2[0][0] = 0;
	m2[0][1] = 1;
	m2[1][0] = 1;
	m2[1][1] = 5;
	matriceP res;
	matprod_MP(m1, m2, res);
	printf("%f\n", res[0][0]);
	printf("%f\n", res[0][1]);
	printf("%f\n", res[1][0]);
	printf("%f\n", res[1][1]);
	return 0;
}

/////////////////////////////////////////////////////////////////
