/*
26/05/2015
Alexis Raguenes
*/

#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "vecteur.h"
#include "MATRICEtype.h"
#include "matrice.h"



//Fonction de multiplication d'une matrice avec un vecteur -> vecteur//

void gaxpyP(matricePF m, vecteurPF v, vecteurPF v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

void gaxpyPD(matricePD m, vecteurPD v, vecteurPD v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

void gaxpyG(matriceGF m, vecteurGF v, vecteurGF v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

void gaxpyGD(matriceGD m, vecteurGD v, vecteurGD v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

/////////////////////////////////////////////////////////////////



//Produit de 2 matrices de meme nature//

void matprod_MP(matricePF m1, matricePF m2,matricePF res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<PETITS;k++){
				tmp = tmp + (m1[j][k] * m2[k][j]);
				printf("tmp = %d\n", tmp);
			}
			res[i][j] = tmp;
			tmp = 0;
		}
	}
}

void matprod_MPD(matricePD m1, matricePD m2,matricePD res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<PETITS;k++){
				tmp = tmp + m1[j][k] * m2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

void matprod_MG(matriceGF m1, matriceGF m2,matriceGF res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<GRANDS;k++){
				tmp = tmp + m1[j][k] * m2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

void matprod_MGD(matriceGD m1, matriceGD m2,matriceGD res){
	int i = 0;
	int j = 0;
	int k = 0;
	int tmp = 0;
	//#pragma omp parallel for
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			//#pragma omp parallel for reduction(+:tmp)
			for(k=0;k<GRANDS;k++){
				tmp = tmp + m1[j][k] * m2[k][j];
			}
			res[i][j] = tmp;
		}
	}
}

/////////////////////////////////////////////////////////////////


//Produit d'une matrice par un float//

void prod_MP(matricePF m, float a,matricePF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m[i][j] * a;
		}
	}	
}

void prod_MPD(matricePD m, float a,matricePD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m[i][j] * a;
		}
	}
}

void prod_MG(matriceGF m, float a,matriceGF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m[i][j] * a;
		}
	}
}

void prod_MGD(matriceGD m, float a,matriceGD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m[i][j] * a;
		}
	}
}

/////////////////////////////////////////////////////////////////


//Somme de 2 matrices//

void somme_MP(matricePF m1, matricePF m2,matricePF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}	
}

void somme_MPD(matricePD m1, matricePD m2,matricePD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<PETITS;i++){
		for( j=0;j<PETITS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}		
}

void somme_MG(matriceGF m1, matriceGF m2,matriceGF res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}		
}

void somme_MGD(matriceGD m1, matriceGD m2,matriceGD res){
	int i = 0;
	int j = 0;
	//#pragma omp parallel for
	for( i=0;i<GRANDS;i++){
		for( j=0;j<GRANDS;j++){
			res[i][j] = m1[i][j] + m2[i][j];
		}
	}		
}

/////////////////////////////////////////////////////////////////

//Fonction de transposition d'une matrice//

void matrans_MP(matricePF m, matricePF mt){

	int i = 0;
	int j = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

void matrans_MPD(matricePD m, matricePD mt){

	int i = 0;
	int j = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

void matrans_MG(matriceGF m, matriceGF mt){

	int i = 0;
	int j = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

void matrans_MGD(matriceGD m, matriceGD mt){

	int i = 0;
	int j = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

/////////////////////////////////////////////////////////////////




