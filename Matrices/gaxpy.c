#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "vecteur.h"
#include "MATRICEtype.h"


//Fonction de multiplication d'une matrice avec un vecteur -> vecteur//

void gaxpyP(matricePF m, vecteurPF v, vecteurPF v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

void gaxpyPD(matricePD m, vecteurPD v, vecteurPD v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

void gaxpyG(matriceGF m, vecteurGF v, vecteurGF v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

void gaxpyGD(matriceGD m, vecteurGD v, vecteurGD v3){
	int i = 0;
	int j = 0;
	int tmp = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			tmp = tmp + m[i][j] * v[j];
		}
		v3[i] = tmp;
	}
}

/////////////////////////////////////////////////////////////////
