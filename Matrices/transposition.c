#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include "vecteur.h"
#include "MATRICEtype.h"
#include "transposition.h"

//Fonction de transposition d'une matrice//

void matrans_MP(matricePF m, matricePF mt){

	int i = 0;
	int j = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

void matrans_MPD(matricePD m, matricePD mt){

	int i = 0;
	int j = 0;
	for(i=0;i<PETITS;i++){
		for(j=0;j<PETITS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

void matrans_MG(matriceGF m, matriceGF mt){

	int i = 0;
	int j = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

void matrans_MGD(matriceGD m, matriceGD mt){

	int i = 0;
	int j = 0;
	for(i=0;i<GRANDS;i++){
		for(j=0;j<GRANDS;j++){
			mt[i][j] = m[j][i];
		}
	}
}

/////////////////////////////////////////////////////////////////
