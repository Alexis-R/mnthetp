#include <stdio.h>
#include <stdlib.h>
//#include <omp.h>
#include <math.h>
#include "LU.h"



int main(){

	matriceP A;
	A[0][0] = 2;
	A[0][1] = 1;
	A[0][2] = 2;
	A[0][3] = 1;
	A[1][0] = 2;
	A[1][1] = 1;
	A[1][2] = 2;
	A[1][3] = 1;
	A[2][0] = 2;
	A[2][1] = 1;
	A[2][2] = 2;
	A[2][3] = 1;
	A[3][0] = 2;
	A[3][1] = 1;
	A[3][2] = 2;
	A[3][3] = 1;
	matriceP L;
	matriceP U;
	
	lu_dsmat(A, L, U);
	
	printf("%f\n", A[0][0]);
	printf("%f\n", A[0][1]);
	printf("%f\n", A[0][2]);
	printf("%f\n", A[0][3]);
	printf("%f\n", L[0][0]);
	printf("%f\n", L[0][1]);
	printf("%f\n", L[1][0]);
	printf("%f\n", L[1][1]);
	printf("%f\n", U[0][0]);
	printf("%f\n", U[0][1]);
	printf("%f\n", U[1][0]);
	printf("%f\n", U[1][1]);
	return 0;
}
