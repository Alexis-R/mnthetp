
#include "MATRICEtype.h"

void lu_dsmat(t_dsmat a, t_dsmat l, t_dsmat u){
	int i, j, k;
	double tmp = 0.0;
	
	//initialisation de u et l
	for(i=0; i<SHORTMAT; i++){
		for(j=0; j<SHORTMAT; j++){
			u[i][j] = 0;
			l[i][j] = 0;
		}
	}
	
	//l = matrice identité
	for(i=0; i<SHORTMAT; i++){
		for(j=0; j<SHORTMAT; j++){
			if(i==j) {l[i][j] = 1;}
		}
	}
	
	
	for(i=0; i<(SHORTMAT-1); i++){
		
		for(j=i; j<SHORTMAT; j++){
			tmp = 0.0;
			for(k=0; k<i; k++){
				tmp = tmp + l[i][k]*u[k][j];
			}
			u[i][j] = a[i][j] - tmp;
		}
		
		for(j=i+1; j<SHORTMAT; j++){
			temp=0.0;
			for(k=0; k<i; k++){
				tmp = tmp + l[j][k]*u[k][i];
			}	
			l[j][i] = (1/u[i][i])*(a[j][i] - tmp);
		}
	}
	
	tmp=0.0;
	for(k=0; k<(SHORTMAT-1); k++){
		tmp = tmp + l[SHORTMAT-1][k]*u[k][SHORTMAT-1];
	}	
	u[SHORTMAT-1][SHORTMAT-1] = a[SHORTMAT-1][SHORTMAT-1] - tmp;
}
