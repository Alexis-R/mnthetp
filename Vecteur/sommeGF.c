/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"
/*void sommeGF(vecteurGF v1,vecteurGF v2,vecteurGF v3);
void readGF(vecteurGF vect);*/
//test de la fonction 
vecteurGF v1;
vecteurGF v2;
vecteurGF v3;

int main(){

readGF(v1);
readGF(v2);
readGF(v3);
sommeGF(v1 ,v2,v3);
return 0;
}
