/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"

//test de la fonction 
vecteurPF v1;
float multi = 2 ;
vecteurPF v3;

int main(){

readPF(v1);

readPF(v3);
prodPF(v1 ,multi,v3);
return 0;
}
