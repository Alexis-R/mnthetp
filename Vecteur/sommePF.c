/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"
/*void sommePF(vecteurPF v1,vecteurPF v2,vecteurPF v3);
void readPF(vecteurPF vect);*/
//test de la fonction 


vecteurPF v1;
vecteurPF v2;
vecteurPF v3;

int main(){

readPF(v1);
readPF(v2);
readPF(v3);
sommePF(v1 ,v2,v3);

return 0;
}
