/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"

//test de la fonction 
vecteurGD v1;
float multi = 2 ;
vecteurGD v3;

int main(){

readGD(v1);

readGD(v3);
prodGD(v1 ,multi,v3);
return 0;
}
