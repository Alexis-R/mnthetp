/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur petits float

int main(){
vecteur v1;
vecteur v3;
read_vect(v1);
to_string(v1);


printf("test multiplication\n");
printf("multiplication par 42 de v1\n");
prod_V(v1, 42,v3);
to_string(v3);

printf("test ecriture");
ecritureF(v3);


return 0;
}

