/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur petits double

int main(){
vecteurD v1;
vecteurD v2;
vecteurD v3;
read_vectD(v1);
to_stringD(v1);
read_vectD(v2);
to_stringD(v2);

printf("test somme\n");
somme_VD(v1,v2,v3);
to_stringD(v3);

printf("test ecriture");
ecritureFD(v3);

return 0;
}

