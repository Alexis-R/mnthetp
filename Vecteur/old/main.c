/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"
#include "main.h"

#define NBVECTEUR 3
int TAILLE = Petits;
//test des fonction de base vecteur petits float

int main(){

vecteur v1;
vecteur v2;
vecteur v3;

read_vect(v1);
to_string(v1);
read_vect(v2);
to_string(v2);
printf("test somme\n");
somme_V(v1,v2,v3);
to_string(v3);
printf("test scalaire\n");
printf("produit scalaire de v1 et v2 (float) %f \n ",scalaire_V( v1,  v2));
printf("test multiplication\n");
printf("multiplication par 42 de v1\n");
prod_V(v1, 42,v3);
to_string(v3);
printf("test ecriture");
ecritureF(v3);


return 0;
}

