/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur grands float

int main(){
gvecteur v1;
gvecteur v2;

gread_vect(v1);
gto_string(v1);
gread_vect(v2);
gto_string(v2);

printf("test scalaire\n");
printf("produit scalaire de v1 et v2 (float) %f \n ",gscalaire_V( v1,  v2));

return 0;
}

