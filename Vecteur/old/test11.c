/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur grand double

int main(){
gvecteurD v1;
gvecteurD v2;
gread_vectD(v1);
gto_stringD(v1);
gread_vectD(v2);
gto_stringD(v2);

printf("test scalaire\n");
printf("produit scalaire de v1 et v2 (float) %f \n ",gscalaire_VD( v1,  v2));

return 0;
}

