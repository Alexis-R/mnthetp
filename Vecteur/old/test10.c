/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur grand double

int main(){
gvecteurD v1;
gvecteurD v2;
gvecteurD v3;
gread_vectD(v1);
gto_stringD(v1);
gread_vectD(v2);
gto_stringD(v2);

printf("test somme\n");
gsomme_VD(v1,v2,v3);
gto_stringD(v3);

printf("test ecriture");
gecritureFD(v3);

return 0;
}

