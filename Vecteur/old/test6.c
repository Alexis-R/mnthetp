/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur petits double

int main(){
vecteurD v1;
vecteurD v2;

read_vectD(v1);
to_stringD(v1);
read_vectD(v2);
to_stringD(v2);

printf("test scalaire\n");
printf("produit scalaire de v1 et v2 (float) %f \n ",scalaire_VD( v1,  v2));

return 0;
}

