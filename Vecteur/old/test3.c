/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur petits float

int main(){
vecteur v1;
vecteur v2;

read_vect(v1);
to_string(v1);
read_vect(v2);
to_string(v2);

printf("test scalaire\n");
printf("produit scalaire de v1 et v2 (float) %f \n ",scalaire_V( v1,  v2));


return 0;
}

