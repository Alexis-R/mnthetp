/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur grand double

int main(){
gvecteurD v1;
gvecteurD v3;
gread_vectD(v1);
gto_stringD(v1);

printf("test multiplication\n");
printf("multiplication par 42 de v1\n");
gprod_VD(v1, 42,v3);
gto_stringD(v3);

printf("test ecriture");
gecritureFD(v3);

return 0;
}

