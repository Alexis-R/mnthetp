/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
*/
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"


#define NBVECTEUR 3

//test des fonction de base vecteur grands float

int main(){
gvecteur v1;
gvecteur v3;
gread_vect(v1);
gto_string(v1);

printf("test multiplication\n");
printf("multiplication par 42 de v1\n");
gprod_V(v1, 42,v3);
gto_string(v3);

printf("test ecriture");
gecritureF(v3);

return 0;
}

