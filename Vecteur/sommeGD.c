/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"
/*void sommeGD(vecteurGD v1,vecteurGD v2,vecteurGD v3);
void readGD(vecteurGD vect);*/
//test de la fonction 
vecteurGD v1;
vecteurGD v2;
vecteurGD v3;


int main(){

readGD(v1);
readGD(v2);
readGD(v3);
sommeGD(v1 ,v2,v3);
return 0;
}
