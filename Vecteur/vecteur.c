/*
Exercice sur les vecteurs 
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015

*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"
#define fichierPF "vectResultPF.txt"
#define fichierPD "vectResultPD.txt"
#define fichierGF "vectResultGF.txt"
#define fichierGD "vectResultGD.txt"

//debut du code de vecteur


//somme de vecteur PETITS PF
void sommePF(vecteurPF v1, vecteurPF v2,vecteurPF v3){
long int i=0;
	#pragma omp parallel private(i)
	#pragma omp for schedule(static)
	for( i=0;i<PETITS;i++){
		v3[i] = v1[i] + v2[i];
	}	
}

//somme de vecteur PETITS PD
void sommePD(vecteurPD v1, vecteurPD v2,vecteurPD v3){
long int i=0;
	for(i=0;i<PETITS;i++){
		v3[i] = v1[i] + v2[i];
	}
}
/////////////////
//produit scalaire retour float      PF
float scalairePF(vecteurPF v1, vecteurPF v2){
long int i=0;
	float scalaire =0;
	for(i=0;i<PETITS;i++){
	scalaire = v1[i] * v2[i] + scalaire;
	}
	return scalaire;
}

//produit scalaire retour double PETITS     PD
float scalairePD(vecteurPD v1, vecteurPD v2){
long int i=0;
	float scalaire =0;
	for(i=0;i<PETITS;i++){
	scalaire = v1[i] * v2[i] + scalaire;
	}
	return scalaire;
}
//////////////////////////
//multiplication d'un vecteur par une constante float PETITS  PF
void prodPF(vecteurPF v1, float a,vecteurPF w){
long int i=0;
	for(i=0;i<PETITS;i++){
	w[i] = v1[i] * a;
	}
}

//multiplication d'un vecteur par une constante double PETITS    PD
void prodPD(vecteurPD v1, float a,vecteurPD w){
long int i=0;

	for(i=0;i<PETITS;i++){
	w[i] = v1[i] * a;
	}
}
////////////:
//fin des calculs
////////////////////////////////::
//affichage float  PETITS   PF
void to_stringPF(vecteurPF v){
long int i=0;

	for(i=0;i<PETITS;i++){
	printf("vecteur PF (%f) , ",v[i]);
	}
	printf("\n\n");
}
//affichage double  PETITS   PD
void to_stringPD(vecteurPD v){  
long int i=0;

	for(i=0;i<PETITS;i++){
	printf("vecteur PD (%f) , ",v[i]);
	}
	printf("\n\n");
}


//fin de l'affichage PETITS 
////////////////////////:
//debut de la lecture float PETITS   PF
void readPF(vecteurPF vect){//PF
long int i=0;


	i=0;
	while(i < PETITS){
       	scanf("%f",&vect[i]);
		i++;
	}
}
// lecture petit double PD
void readPD(vecteurPD vect){
	long int i=0;
	while(i < PETITS){
       scanf("%lf",&vect[i]);
		i++;
	}
}
//fin de la lecture PETITS 
////////////////////////////////////////////:



//write dans un fichier float  PETITS   PF
void writePF(vecteurPF vect){
long int i=0;

    FILE* fichier = NULL;

    fichier = fopen(fichierPF, "w+");
	i=0;
    if (fichier != NULL){
		while(i < PETITS){
        	fprintf(fichier, "%f",vect[i]);
			i++;
		}

        fclose(fichier);
		}
}
//write dans un fichier double  PETITS   PD
void writePD(vecteurPD vect){
long int i=0;

    FILE* fichier = NULL;//on initialise le fichier

    fichier = fopen(fichierPD, "w+");
	i=0;
    if (fichier != NULL){
		while(i < PETITS){
        	fprintf(fichier, "%lf",vect[i]);
			i++;
		}

        fclose(fichier);
		}
}

//fin write dans un fichier PETITS


//fin du code PETITS


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//somme de gvecteur GRANDS GF
void sommeGF(vecteurGF v1, vecteurGF v2,vecteurGF v3){
long int i=0;

	for( i=0;i<GRANDS;i++){
		v3[i] = v1[i] + v2[i];
	}	
}

//somme de gvecteurD GRANDS GD
void sommeGD(vecteurGD v1, vecteurGD v2,vecteurGD v3){
long int i=0;

	for(i=0;i<GRANDS;i++){
		v3[i] = v1[i] + v2[i];
	}
}

//produit scalaire retour float GRANDS  GF
float scalaireGF(vecteurGF v1, vecteurGF v2){
long int i=0;

	float scalaire =0;
	for(i=0;i<GRANDS;i++){
	scalaire = v1[i] * v2[i] + scalaire;
	}
	return scalaire;
}

//produit scalaire retour double GRANDS   GD
float scalaireGD(vecteurGD v1, vecteurGD v2){
long int i=0;

	float scalaire =0;
	for(i=0;i<GRANDS;i++){
	scalaire = v1[i] * v2[i] + scalaire;
	}
	return scalaire;
}

//multiplication d'un gvecteur par une constante float GRANDS     GF
void prodGF(vecteurGF v1, float a,vecteurGF w){
long int i=0;

	for(i=0;i<GRANDS;i++){
	w[i] = v1[i] * a;
	}
}

//multiplication d'un gvecteur par une constante double GRANDS     GD
void prodGD(vecteurGD v1, float a,vecteurGD w){
long int i=0;

	for(i=0;i<GRANDS;i++){
	w[i] = v1[i] * a;
	}
}

//fin des calculs

//affichage float  GRANDS    GF
void to_stringGF(vecteurGF v){
long int i=0;

	for(i=0;i<GRANDS;i++){
	printf("gvecteur (%f) , ",v[i]);
	}
	printf("\n\n");
}
//                                 GD
void to_stringGD(vecteurGD v){
long int i=0;

	for(i=0;i<GRANDS;i++){
	printf("gvecteurD (%lf) , ",v[i]);
	}
	printf("\n\n");
}


//fin de l'affichage GRANDS 

//debut de la lecture float GRANDS      GF
void readGF(vecteurGF vect){
long int i=0;


	i=0;
	while(i < GRANDS){
       	scanf("%f",&vect[i]);
		i++;
	}
}
//                           GD
void readGD(vecteurGD vect){
long int i=0;

	i=0;
	while(i < GRANDS){
       	scanf("%lf",&vect[i]);
		i++;
	}
}
//fin de la lecture GRANDS 


//write dans un fichier float  GRANDS            GF
void writeGF(vecteurGF vect){
long int i=0;

    FILE* fichier = NULL;

    fichier = fopen(fichierGF, "w+");
	i=0;
    if (fichier != NULL){
		while(i < GRANDS){
        	fprintf(fichier, "%f",vect[i]);
			i++;
		}

        fclose(fichier);
		}
}
//                                       GD
void writeGD(vecteurGD vect){
long int i=0;

    FILE* fichier = NULL;//on initialise le fichier

    fichier = fopen(fichierGD, "w+");// le nom du fichier FICHIER est definie par un #define au debut du code
	i=0;
    if (fichier != NULL){
		while(i < GRANDS){
        	fprintf(fichier, "%lf",vect[i]);
			i++;
		}

        fclose(fichier);
		}
}

//fin write dans un fichier GRANDS

//fin du code GRANDS
//affiche la valeur de petit et grand
void affiche(){
printf("Valeur d'un PETITS vecteur:%d \n",PETITS);
printf("Valeur d'un GRANDS vecteur:%d \n",GRANDS);
}
//

//fin du code
