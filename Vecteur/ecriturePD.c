/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"

//test de la fonction 
vecteurPD v1;
vecteurPD v2;
vecteurPD v3;

int main(){

readPD(v1);
readPD(v2);
readPD(v3);
ecriturePD(v1 ,v2,v3);
return 0;
}
