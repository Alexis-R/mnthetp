void multiplicationFloatMatriceSSE(fmatrice mat1,fmatrice mat2,fmatrice mat3){
	__m128 d1, d2, d3;

	fmatrice mat2hori;
	transposeeFloatMatrice(mat2, mat2hori);

	int i = 0;
	int j = 0;
	int k = 0;

	float prendre;

	for(i = 0; i < TAILLE_MATRICE_FLOAT; i++){
		for(j = 0; j < TAILLE_MATRICE_FLOAT; j++){
			mat3[i][j] = 0.0;

			for (k = 0; k<TAILLE_MATRICE_FLOAT; k+=4){
				d1=_mm_loadu_ps(mat1[i]+k);
				d2=_mm_loadu_ps(mat2hori[j]+k);
				d3=_mm_mul_ps(d1,d2);

				d1 = _mm_hadd_ps(d3,d3);
				d2 = _mm_hadd_ps(d1,d1);

				_mm_store_ss(&prendre, d2);

				mat3[i][j] += prendre;
			}
		}
	}
}


----------------------------------------------------------------------------------------------------------------------------

void mats_add_sse(mats m1, mats m2, mats res) {
	__m128 r1, r2, r3;

	int l, c;
	for(l = 0; l < SMALL_SIZE; l++) {
		for(c = 0; c < SMALL_SIZE; c+= 4) {
			r1 = _mm_load_ps( m1[l] + c );
			r2 = _mm_load_ps( m2[l] + c );
			r3 = _mm_add_ps( r1 , r2 );
			_mm_store_ps( res[l] + c , r3 );
		}
	}
}


---------------------------------
