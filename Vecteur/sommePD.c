/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"
vecteurPD v1;
vecteurPD v2;
vecteurPD v3;


int main(){

readPD(v1);
readPD(v2);
readPD(v3);
sommePD(v1,v2,v3);
return 0;
}

