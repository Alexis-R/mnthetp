#Alexis Raguenes
#Jordan Ellapin


LISTVECT="Vect.txt"

make

echo "/****************************************"
echo "     execution des tests  de Vecteur "
echo "****************************************/"
echo -e '\n'
./affiche
echo "-"
echo "Addition PETITS float"
time ./sommePF < $LISTVECT
echo "-"
echo "Addition PETITS double"
time ./sommePD < $LISTVECT
echo "-"
echo "Addition GRANDS float"
time ./sommeGF < $LISTVECT
echo "-"
echo "Addition GRANDS double"
time ./sommeGD < $LISTVECT


echo "-"
echo "Produit PETITS float"
time ./prodPF < $LISTVECT
echo "-"
echo "Produit PETITS double"
time ./prodPD < $LISTVECT
echo "-"
echo "Produit GRANDS float"
time ./prodGF < $LISTVECT
echo "-"
echo "Produit GRANDS double"
time ./prodGD < $LISTVECT
echo "-"



echo "-"
echo "Scalaire PETITS float"
time ./scalairePF < $LISTVECT
echo "-"
echo "Scalaire PETITS double"
time ./scalairePD < $LISTVECT
echo "-"
echo "Scalaire GRANDS float"
time ./scalaireGF < $LISTVECT
echo "-"
echo "Scalaire GRANDS double"
time ./scalaireGD < $LISTVECT
echo "-"

