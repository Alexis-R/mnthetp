/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"

//test de la fonction 
vecteurPD v1;
float multi = 2 ;
vecteurPD v3;

int main(){

readPD(v1);

readPD(v3);
prodPD(v1 ,multi,v3);
return 0;
}
