/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.c"

//test de la fonction 
vecteurPF v1;
vecteurPF v2;
vecteurPF v3;

int main(){

readPF(v1);
readPF(v2);
readPF(v3);
ecriturePF(v1 ,v2,v3);
return 0;
}
