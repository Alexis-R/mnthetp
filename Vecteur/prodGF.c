/*
Exercice sur les vecteurs
Alexis RAGUENES
Jordan ELLAPIN
Date : 22/04/2015
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "vecteur.h"

//test de la fonction 
vecteurGF v1;
float multi = 2 ;
vecteurGF v3;

int main(){

readGF(v1);

readGF(v3);
prodGF(v1 ,multi,v3);
return 0;
}
